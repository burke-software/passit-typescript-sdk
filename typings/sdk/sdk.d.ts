
/**
 * The secret interface
 */
interface Secret {

    /**
     * The group id if any
     * @type {number}
     */
    group?:number,

    /**
     * The public key as a pem used to encrypt the hidden data
     * @type {string}
     */
    public_key?:string

    /**
     * The user id if any
     * @type {number}
     */
    user?:number,

    /**
     * The encrypted key/value hidden data
     * @type {object}
     */
    hidden_data:({[key:string]:string}),

    /**
     * The encrypted aes key
     * @type {string}
     */
    encrypted_key:string

}

/**
 * The interface for new secrets that will be sent to the server
 */
interface NewSecret {

    /**
     * The name of the secret
     * @type {string}
     */
    name:string,

    /**
     * The type of the secret
     * @type {string}
     */
    type:string,

    /**
     * The public info about the secret
     * @type {object}
     */
    visible_data:({[key:string]:string}),

    /**
     * The encrypted data
     * @type {secret[]}
     */
    secrets:Secret[]

}

/**
 * The object returned from the user lookup
 */
interface UserLookup {

    /**
     * The user id
     * @type {number}
     */
    id:number,

    /**
     * The users first name
     * @type {string}
     */
    first_name:string,

    /**
     * The users last name
     * @type {string}
     */
    last_name:string,

    /**
     * The users public key as pem
     * @type {string}
     */
    public_key:string

}

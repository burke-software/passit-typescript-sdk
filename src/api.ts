
/**
 * The response of an api call
 */
export class Response {

    /**
     * @param {XMLHttpRequest} _request - the xml http request
     */
    constructor(private _request:XMLHttpRequest){

    }

    /**
     * Convert the reponse to text
     * @return {string} the response as a string
     */
    public text(): string {
        return this._request.responseText;
    }

    /**
     * Convert the response to json
     * @return {object} the response as a json object
     */
    public json(): any {
        return JSON.parse(this._request.responseText);
    }

}

/**
 * Class for making Api Calls
 */
export default class Api {

    /**
     * The token for logged in requests
     * @type {string}
     */
    public token:string;

    /**
     * @param {string} baseUrl - the base url of the api
     */
    constructor(private _baseUrl?:string) {
        
    }

    /**
     * Send a general request
     * @param {string} method - the method of the request
     * @param {string} uri - the uri to send the request to
     * @param {object} headers - the request headers in key:value format
     * @param {string} body - the request body if any
     * @return {Promise<Response>} the response of the request
     */
    public send(method:string, uri:string, headers?:any, body?:string): Promise<Response> {
        return new Promise((resolve, reject)=>{
            //create a new request
            let xhr:XMLHttpRequest = new XMLHttpRequest();
            xhr.open(method, this._baseUrl + uri, true);
            //if there is a token add it to the headers
            if(this.token){
                    xhr.setRequestHeader('Authorization','Token ' + this.token);
            }
            //set the headers
            if(headers){
                for(let key in headers){
                    xhr.setRequestHeader(key, headers[key]);
                }
            }
            //function for returning the request
            xhr.onreadystatechange = () => {
                if(xhr.readyState === XMLHttpRequest.DONE){
                    let response:Response = new Response(xhr);
                    //if the code is less then 400 resolve else reject
                    if(xhr.status < 400){
                        resolve(response);
                    } else {
                        reject(response);
                    }
                }
            };
            //send
            xhr.send(body);
        });
    }

    /**
     * Send a json get request
     * @param {string} uri - the uri to send the request to
     * @return {Promise<Response>} the response in a promise
     */
    jsonGet(uri:string): Promise<Response> {
        let headers:any = {
            'Accept': 'application/json'
        };
        return this.send('GET', uri, headers);
    }

    /**
     * Send a json post request
     * @param {string} uri - the uri to send the request to
     * @param {object} body - the body of the request
     * @return {Promise<Response>} the response in a promise
     */
    jsonPost(uri:string, body?:any): Promise<Response> {
        let headers:any = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };
        if(body){
            body = JSON.stringify(body);
        } else {
            body = null;
        }
        return this.send('POST', uri, headers, body);
    }

    /**
     * Send a json put request
     * @param {string} uri - the uri to send the request to
     * @param {object} body - the body of the request
     * @return {Promise<Response>} the response in a promise
     */
    jsonPut(uri:string, body?:any): Promise<Response> {
        let headers:any = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };
        if(body){
            body = JSON.stringify(body);
        } else {
            body = null;
        }
        return this.send('PUT', uri, headers, body);
    }

    /**
     * Send a json path request
     * @param {string} uri - the uri to send the request to
     * @param {object} body - the body of the request
     * @return {Promise<Response>} the response in a promise
     */
    jsonPatch(uri:string, body?:any): Promise<Response> {
        let headers:any = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };
        if(body){
            body = JSON.stringify(body);
        } else {
            body = null;
        }
        return this.send('PATCH', uri, headers, body);
    }

    /**
     * Send a login request
     * @param {string} uri - the uri to send the request to
     * @param {string} email - the users email
     * @param {string} hash - the hash of the password
     * @return {Promise<Response>} the response in a promise
     */
    login(uri:string, email:string, hash:string): Promise<Response> {
        let base = btoa(email + ':' + hash);
        let headers = {
            'Accept': 'application/json',
            'Authorization': 'Basic ' + base
        };
        return this.send('POST', uri, headers);
    }

}

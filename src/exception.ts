
/**
 * Class for throwing exceptions
 */
export default class Exception extends Error {

    /**
     * The reponse text of a request
     * @type {string|object}
     */
    public res:any;

    /**
     * Init
     * @param {string|object} res - the response json or text
     * @param {string} message - the error message
     */
    constructor(private _res:any, private _message:string){
        super(_message);
        this.res = _res;
        this.message = _message;
        this.name = 'Passit SDK Error';
    }

}

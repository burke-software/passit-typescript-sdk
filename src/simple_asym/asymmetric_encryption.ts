
/**
 * Import and declare node forge
 */
declare var forge:any;
import '../../node_modules/node-forge/js/forge.bundle.js';

/**
 * Import and declare fernet
 */
declare var fernet:any;
import '../../node_modules/fernet/fernetBrowser.js';

/**
 * Errors
 */

/**
 *  * Error for missing aes key
 *   * @type {Error}
 *    */
var MissingAESException:Error = new Error('Missing AES key. Set or generate one');

/**
 *  * Error for missing public key
 *   * @type {Error}
 *    */
var MissingRSAPublicException:Error = new Error('Missing public RSA key. Set or generate one to use RSA encryption');

/**
 *  * Error for missing private key
 *   * @type {Error}
 *    */
var MissingRSAPrivateException:Error = new Error('Missing private RSA key. Set or generate one to use RSA decrypt');


/**
 * Class for handeling the encryption
 */
export default class Asym {

    /**
     * The worker for background key generation
     * @type {Worker}
     */
    private _worker:Worker;

    /**
     * The pool of public and private keys to use
     * @type <Array({public_key:string, private_key:string})>
     */
    private _keyPool:Array<({public_key:string, private_key:string})> = [];

    /**
     * The public key
     * @type {pki}
     */
    private _public_key:any;

    /**
     * The private key
     * @type {pki}
     */
    private _private_key:any;

    /**
     * The aes key
     * @type {string}
     */
    private _aes_key:string;

    /**
     * The saved state of the current asym
     * @type {object}
     */
    private _state:({
        public_key:string,
        private_key:string,
        aes_key:string,
        password:string
    }) = {
        public_key:'',
        private_key:'',
        aes_key:'',
        password:''
    };

    /**
     * The private key password
     * @type {string}
     */
    public password:string;

    /**
     * Get the aes key
     * @return {string} the aes key
     */
    public get aes_key(): string {
        return this._aes_key;
    }

    /**
     * Get the public key
     * @return {string} the public key as a pem string
     */
    public get public_key(): string {
        return forge.pki.publicKeyToPem(this._public_key);
    }

    /**
     * Get the private key
     * @return {string} the private key as a pem string encrypted if there is a password
     */
    public get private_key(): string {
        if(this.password){
            return forge.pki.encryptRsaPrivateKey(this._private_key, this.password);
        } else {
            return forge.pki.privateKeyToPem(this._private_key);
        }
    }

    /**
     * Init
     * @param {number} numIterations - the default number of iterations to use for hashing
     * @param {number} maxKeys - the max number of key pairs to have at once
     * @param {number} keyBits - the bits to use in key generation
     * @param {string} workerFile - the location of the web worker script
     */
    constructor(private _numIterations, private _maxKeys?:number, private _keyBits?:number, private _workerFile?:string) {
        //check if the worker file is defined if not do not start worker
        if(_workerFile){
            this._worker = new Worker(_workerFile);
            this._generate();
            /**
             * The worker response
             * @param {event} the message event
             * @property {object} data - the public and private key
             */
            this._worker.onmessage = (event) =>{
                this._keyPool.push(JSON.parse(event.data));
                this._generate();
            };
        }
    }

    /**
     * Genertae the key pairs
     */
    private _generate(): void {
        if(this._keyPool.length < this._maxKeys){
            //send message to worker
            this._worker.postMessage(JSON.stringify({bits: this._keyBits}));
        }
    }

    /**
     * Generate the fernet key
     * @return {string} the base64 encoded key
     */
    private _generate_key(): string {
        let bytes = forge.random.getBytesSync(32);
        return forge.util.encode64(bytes);
    }

    /**
     * Generate a random passphrase
     * @param {number} N - the size of the passphrase defaults to 255
     * @return {string} the passphrase
     */
    private _generate_passphrase(N:number=255): string {
        let text = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for(let i=0; i<N; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

    /**
     * Delete the web worker
     */
    public terminate(): void {
        this._worker.terminate();
    }

    /**
     * Save the current state
     */
    public save(): void {
        this._state = {
            public_key: this._public_key,
            private_key: this._private_key,
            aes_key: this._aes_key,
            password: this.password
        };
    }

    /**
     * Load the state from the state object
     */
    public load(): void {
        this._public_key = this._state.public_key;
        this._private_key = this._state.private_key;
        this._aes_key = this._state.aes_key;
        this.password = this._state.password;
        this._state = {
            public_key:'',
            private_key:'',
            aes_key:'',
            password:''
        };
    }

    /**
     * Genertae a hash from a password salt and number of iterations
     * @param {string} password - the password to hash
     * @param {string} salt - the salt as a hex string
     * @param {number} numIterations the number of iterations to use for hashing
     * @return {Object(hash:string, salt:string)} the hash and salt in an object
     */
    public generate_hash(password:string, salt?:string, numIterations:number=this._numIterations): ({hash:string, salt:string}) {
        let saltBytes;
        if(salt){
            saltBytes = forge.util.hexToBytes(salt);
        } else {
            saltBytes = forge.random.getBytesSync(128);
        }
        let derivedKey = forge.pkcs5.pbkdf2(password, saltBytes, numIterations, 16);
        return {hash: forge.util.bytesToHex(derivedKey), salt: forge.util.bytesToHex(saltBytes)};
    }

    /**
     * Set the private key
     * @param {string|object} private_key - the private key as a pem string or forge object
     * @param {string} passphrase - the passphrase to decrypt the private key
     */
    public set_private_key(private_key:any, passphrase?:string): void {
        if(passphrase){
            this._private_key = forge.pki.decryptRsaPrivateKey(private_key, passphrase);
        } else if(typeof private_key === 'string'){
            this._private_key = forge.pki.privateKeyFromPem(private_key);
        } else {
            this._private_key = private_key;
        }
    }

    /**
     * Set the public key
     * @param {string|object} public_key - the public key as a pem string or forge object
     */
    public set_public_key(public_key:any): void {
        if(typeof public_key === 'string'){
            this._public_key = forge.pki.publicKeyFromPem(public_key)
        } else {
            this._public_key = public_key;
        }
    }

    /**
     * Generate the public and private keys
     * @param {string} password - the password to encrypt the private key pem file
     * @param {boolean} set - if true set the new keys as the current keys
     * @return {Promise({public_key:string, private_key:string}) the public and private key in an object
     */
    public make_rsa_keys(password?:string, set:boolean=true): Promise<({public_key:string, private_key:string})> {
        if(password && set){
            this.password = password;
        }
        return new Promise((resolve, reject)=>{
            /**
             * Get the new keys from the key pool
             */
            const getKeys:Function = () =>{
                let keys = this._keyPool.shift();
                //generate a new key pair
                this._generate();
                if(set){
                    this.set_public_key(keys.public_key);
                    this.set_private_key(keys.private_key);
                    return resolve({public_key: this.public_key, private_key: this.private_key});
                } else {
                    return resolve({public_key: keys.public_key, private_key: keys.private_key});
                }
            };
            
            /**
             * Check if there are keys in the key pool
             */
            const checkPool:Function = () =>{
                if(this._keyPool.length > 0){
                    getKeys();
                } else {
                    setTimeout(()=>{
                        checkPool();
                    }, 100);
                }
            };

            checkPool();

        });
    }

    /**
     * Wrapper for making the public and private keys with an auto generated passphrase
     * @return {Promise({public_key:string, private_key:string}) the public and private key in an object
     */
    public make_rsa_keys_with_passphrase(): Promise<({public_key:string, private_key:string})> {
        let passphrase = this._generate_passphrase();
        return this.make_rsa_keys(passphrase);
    }

    /**
     * Encrypt plain text with the public key
     * @param {string} text - the text to encrypt
     * @param {boolean} use_base64 - if true encode the ciphertext as base64
     * @return {string} the ciphertext
     */
    public rsa_encrypt(text:string, use_base64?:boolean): string {
        if(!this._public_key){
            throw MissingRSAPublicException;
        }
        let encrypted = this._public_key.encrypt(text, 'RSA-OAEP', {
                mgf1: {
                    md: forge.md.sha1.create()
                }
        });
        if(use_base64){
            encrypted = forge.util.encode64(encrypted);
        }
        return encrypted;
    }

    /**
     * Decrypt ciphertext with the private key
     * @param {string} ciphertext - the ciphertext to decrypt
     * @param {boolean} use_base64 - if the ciphertext is base64 decode it first
     * @return {string} the decrypted text
     */
    public rsa_decrypt(ciphertext:string, use_base64?:boolean): string {
        if(!this._private_key){
            throw MissingRSAPrivateException;
        }
        if(use_base64){
            ciphertext = forge.util.decode64(ciphertext);
        }
        let text = this._private_key.decrypt(ciphertext, 'RSA-OAEP', {
            mgf1: {
                md: forge.md.sha1.create()
            }
        });
        return text;
    }

    /**
     * Set the aes key
     * @param {string} aes_key - the new aes key
     */
    public set_aes_key(aes_key): void {
        this._aes_key = aes_key;
    }

    /**
     * Generate a new aes key
     * @return {string} the new aes key
     */
    public make_aes_key(): string {
        let key = this._generate_key();
        this.set_aes_key(key);
        return this.aes_key;
    }

    /**
     * Get an aes key encrypted by the public key
     * @param {string|object} public_key - the public key to use as a pem string or forge object
     * @param {boolean} use_base64 - convert the result to base 64
     * @return {string} the encrypted aes key
     */
    public get_encrypted_aes_key(public_key:string, use_base64?:boolean): string {
        const public_asym = new Asym(this._numIterations);
        public_asym.set_public_key(public_key);
        let encrypted_key = public_asym.rsa_encrypt(this._aes_key);
        if(use_base64){
            encrypted_key = forge.util.encode64(encrypted_key);
        }
        return encrypted_key;
    }

    /**
     * Set the aes key from ciphertext
     * @param {string} aes_key - the encrypted aes key
     * @param {boolean} use_base64 - if the encrypted aes key is base64 encoded
     */
    public set_aes_key_from_encrypted(aes_key:string, use_base64?:boolean): void {
        if(use_base64){
            aes_key = forge.util.decode64(aes_key);
        }
        let newKey = this.rsa_decrypt(aes_key);
        this.set_aes_key(newKey);
    }

    /**
     * Encrypt text using the aes key
     * @param {string} text - the text to encrypt
     * @return {string} the ciphertext
     */
    public encrypt(text:string): string {
        if(!this._aes_key){
            throw MissingAESException;
        }
        let token = new fernet.Token({
            secret: new fernet.Secret(this._aes_key),
        });
        return token.encode(text);
    }
    
    /**
     * Decrypt text using the aes key
     * @param {string} ciphertext - the ciphertext to decrypt
     * @return {string} the decrypted text
     */
    public decrypt(ciphertext:string): string {
        if(!this._aes_key){
            throw MissingAESException;
        }
        let token = new fernet.Token({
            secret: new fernet.Secret(this._aes_key),
            token: ciphertext,
            ttl: 0
        });
        return token.decode();
    }

}

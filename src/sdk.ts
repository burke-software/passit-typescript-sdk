
import {Config, ConfigObj} from './config';
import Exception from './exception';
import Api, {Response} from './api';
import Asym from './simple_asym/asymmetric_encryption';


/**
 * Class for the base methods of the sdk
 */
export default class passitSDK {

    /**
     * The options for the sdk
     * @type {Object}
     */
    private _options:ConfigObj = Config;

    /**
     * The class for making api calls
     * @type {Api}
     */
    public api:Api;

    /**
     * Class for encryption
     * @type {Asym}
     */
    public asym:Asym;

    /**
     * @param {Object} options - the options for the sdk can be any value from the config object
     */
    constructor(private _userOptions:any) {
        Object.assign(this._options, _userOptions);
        //create the api
        this.api = new Api(this._options.URL);
        //crate the asym
        this.asym = new Asym(this._options.DEFAULT_HASH_ITERATIONS, this._options.MAXKEYS, this._options.KEYBITS, this._options.WORKERFILE);
    }

    /**
     * Hash the users password
     * @param {string} password - the password to hash
     * @param {string} salt - the salt if any as a hex string
     * @param {number} iterations - the iterations used for hashing
     * @return {Object(hash:string, salt:string)} the hash and salt in an object
     */
    private _hash_password(password:string, salt?:string, iterations:number=this._options.DEFAULT_HASH_ITERATIONS): ({hash:string, salt:string}) {
        return this.asym.generate_hash(password, salt, iterations);
    }

    /**
     * Get another users public key
     * @param {number} user_id - the users id
     * @return {Promise<string>} the users public key in a promise
     */
    private _get_user_public_key(user_id:number): Promise<string> {
        return new Promise((resolve, reject)=>{
            this.api.jsonGet(`/api/user-public-key/${user_id}/`).then((res)=>{
                let data:any = res.json();
                resolve(data.public_key);
            }).catch((err)=>{
                reject(new Exception(err.json(), 'Unable to get user public key'));
            });
        });
    }

    /**
     * Look up a users id and other info based on an email
     * @param {string} email - the users email
     * @return {Promise<UserLookup>} a promise with the users info
     */
    private _user_lookup(email:string): Promise<any> {
        return new Promise((resolve, reject)=>{
            this.api.jsonGet(`/api/user-lookup/${email}`).then((res)=>{
                resolve(res.json());
            }).catch((err)=>{
                reject(new Exception(err.json(), 'User not found'));
            });
        });
    }

    /**
     * Get and decrypt the client own keys
     * @param {string} password - the users password
     * @return {Promise({public_key:string, private_key:string)} a promise with the public and private keys in an object
     */
    private _get_own_keys(password:string): Promise<({public_key:string, private_key:string})> {
        return new Promise((resolve, reject)=>{
            this.api.jsonGet('/api/users/').then((res)=>{
                let data:any = res.json();
                let public_key = data[0].public_key;
                let private_key = data[0].private_key;
                this.asym.set_public_key(public_key);
                this.asym.set_private_key(private_key, password);
                //return the private key as an unencrypted pem string
                let tmpPass:string = this.asym.password;
                this.asym.password = null;
                let tmpPrivate:string = this.asym.private_key;
                this.asym.password = tmpPass;
                resolve({public_key: this.asym.public_key, private_key: tmpPrivate});
            }).catch((err)=>{
                reject(new Exception(err.json(), 'Unable to get private user info'));
            });
        });
    }

    /**
     * Get a groups private key
     * @param {number} group_id - the id of the group we want the key for
     * @return {Promise<string>} a promise with the private key as a pem string
     */
    private _get_group_private_key(group_id:number): Promise<string> {
        return new Promise((resolve, reject)=>{
            this.api.jsonGet(`/api/groups/${group_id}/`).then((res)=>{
                let data:any = res.json();
                let group_aes:string = data.my_encrypted_aes_key;
                let group_private_key:string = data.my_encrypted_private_key;
                this.asym.save();
                //set the aes key for decrypting the private key
                this.asym.set_aes_key_from_encrypted(group_aes, true);
                //get the key as a pem string
                let private_key:string = this.asym.decrypt(group_private_key);
                //reload the asym state
                this.asym.load();
                resolve(private_key);
            }).catch((err)=>{
                reject(new Exception(err.json(), 'Group not found'));
            });
        });
    }

    /**
     * Get a groups public key
     * @param {number} group_id - the groups id
     * @return {Promise<string>} a promise with the public key as a pem string
     */
    private _get_group_public_key(group_id:number): Promise<string> {
        return new Promise((resolve, reject)=>{
            this.api.jsonGet(`/api/groups/${group_id}/`).then((res)=>{
                let data:any = res.json();
                resolve(data.public_key);
            }).catch((err)=>{   
                reject(new Exception(err.json(), 'Unable to get group public key'));
            });
        });
    }

    /**
     * Decrypt a secret
     * @param {string} encrypted_key - the encrypted aes key used to decrypt the secrets
     * @param {string|object} encrypted_secret - the secret to decrypt or the secrets to decrypt in an object
     * @param {string} group_private_key - the group private key as a pem string
     * @return {string|object} the single decrypted secret or the values decrypted in an object
     */
    private _decrypt_secrets(encrypted_key:string, encrypted_secret:any, group_private_key?:string): any {
        if(group_private_key){
            //save the current asym state
            this.asym.save();
            this.asym.set_private_key(group_private_key);
        }
        //set the aes key
        this.asym.set_aes_key_from_encrypted(encrypted_key, true);
        if(typeof encrypted_secret === 'string'){
            //decrypt and return a single secret
            let retValue:string = this.asym.decrypt(encrypted_secret);
            if(group_private_key){
                this.asym.load();
            }
            return retValue;
        } else {
            //decrypt secrets in an object
            for(let key in encrypted_secret){
                encrypted_secret[key] = this.asym.decrypt(encrypted_secret[key]);
            }
            if(group_private_key){
                this.asym.load();
            }
            return encrypted_secret;
        }
    }
    
    /**
     * Encrypt the value of an object of secrets in key/value form
     * @param {string} public_key - the public key if any to use for the encryption
     * @param {object} secrets - the secrets to encrypt
     * @return {object({encrypted_key:string, encrypted_secrets:object}) return an object with the new aes key and the encrypted secrets in an object
     */
    private _encrypt_secrets(secrets:any, public_key?:string): ({encrypted_key:string, encrypted_secrets:any}) {
        if(public_key){
            this.asym.save();
            this.asym.set_public_key(public_key);
        }
        let key:string = this.asym.make_aes_key();
        //encrypt the aes key
        let encrypted_key:string = this.asym.rsa_encrypt(key, true);
        let encrypted_secrets:any = {};
        for(let key in secrets){
            encrypted_secrets[key] = this.asym.encrypt(secrets[key]);
        }
        if(public_key){
            this.asym.load();
        }
        return {
            encrypted_key,
            encrypted_secrets
        };
    }

    /**
     * Check if a email is avaible for signup
     * @param {string} email - the email to check
     * @return {Promise<boolean>} true if the username is available
     */
    public is_username_available(email:string): Promise<boolean> {
        return new Promise((resolve, reject)=>{
            this.api.jsonGet(`/api/user-public-auth/${email}/`).then((res)=>{
                let data:any = res.json();
                resolve(data.available);
            }).catch((err)=>{
                reject(new Exception(err.json(), 'Unable to check username'));
            });
        });
    }

    /**
     * Sign the user up
     * @param {string} email - the email used for signup
     * @param {string} password - the users password
     * @param {string} first_name - the users first name
     * @param {string} last_name - the users last name
     * @param {number} iterations - the iterations used for hashing
     * @return {Promise<number>} return the users new id
     */
    public sign_up(email:string, password:string, first_name:string='', last_name:string='', iterations:number=this._options.DEFAULT_HASH_ITERATIONS): Promise<number> {
        return new Promise((resolve, reject)=>{
            //get the client hash and salt
            let hashObj:({hash:string, salt:string}) = this._hash_password(password);
            //get new keys for the client
            this.asym.make_rsa_keys(password).then((keys)=>{
                /**
                 * The data we are sending for the sign up
                 */
                let data:({
                    email:string,
                    password:string,
                    public_key:string,
                    private_key:string,
                    client_salt:string
                }) = {
                    email: email,
                    password: hashObj.hash,
                    public_key: keys.public_key,
                    private_key: keys.private_key,
                    client_salt: hashObj.salt
                };
                //send to the api
                return this.api.jsonPost('/api/users/', data);
            }).then((res)=>{
                let jsonObj:any = res.json();
                resolve(jsonObj.id);
            }).catch((err)=>{
                reject(new Exception(err.json(), 'Unable to check username'));
            });
        });
    }

    /**
     * Log the user in
     * @param {string} email - the users email
     * @param {string} password - the users password
     * @return {Promise}
     */
    public log_in(email:string, password:string): Promise<any> {
        return new Promise((resolve, reject)=>{
            this.api.jsonGet(`/api/user-public-auth/${email}/`).then((res)=>{
                let data:any = res.json();
                //hash the users password
                let hashed_passwordObject:({hash:string, salt:string}) = this._hash_password(password, data.client_salt, data.client_iterations);
                return this.api.login('/api/auth/login/', email, hashed_passwordObject.hash);
            }).then((res)=>{
                let data:any = res.json();
                //set the login token
                this.api.token = data.token;
                //set the users keys
                this.asym.set_public_key(data.user.public_key);
                this.asym.set_private_key(data.user.private_key, password);
                resolve();
            }).catch((err)=>{
                if(err.res){
                    reject(err);
                } else {
                    reject(new Exception(err.json(), 'Unable to log in'));
                }
            });
        });
    }

    /**
     * Create a new group
     * @param {string} name - the new group name
     * @param {string} slug - the new group slug
     * @return {Promise<Response>} a promise with the reponseof the create group
     */
    public create_group(name:string, slug:string): Promise<Response> {
        return new Promise((resolve, reject)=>{
            //get a pair of keys from the key pool
            this.asym.make_rsa_keys(null, false).then((keys)=>{
                this.asym.save();
                //generate a new aes key
                this.asym.make_aes_key();
                //encrypt the aes key with the users public key
                let aes_key:string = this.asym.get_encrypted_aes_key(this.asym.public_key, true);
                //encrypt the private key with the new aes key
                let encrypted_private_key:string = this.asym.encrypt(keys.private_key);
                //the data we are sending to the server
                let data:({
                    name:string,
                    slug:string,
                    public_key:string,
                    encrypted_aes_key:string,
                    encrypted_private_key:string
                }) = {
                    name: name,
                    slug: slug,
                    public_key: keys.public_key,
                    encrypted_aes_key: aes_key,
                    encrypted_private_key: encrypted_private_key
                };
                this.asym.load();
                return this.api.jsonPost('/api/groups/', data);
            }).then((res)=>{
                resolve(res);
            }).catch((err)=>{
                if(err.res){
                    reject(err);
                } else {
                    reject(new Exception(err.json(), 'Couldn\'t create group'));
                }
            });
        });
    }

    /**
     * Add a new user to an existing group
     * @param {number} group_id - the id of the group to add to user to
     * @param {number|string} invitee - the id or email of the user to add
     * @return {Promise<Response>} a promise with the response of the action
     */
    public add_user_to_group(group_id:number, invitee:any): Promise<Response> {
        return new Promise((resolve, reject)=>{
            let group_private_key:string;
            //get the groups private key
            this._get_group_private_key(group_id).then((privKey)=>{
                group_private_key = privKey;
                this.asym.save();
                this.asym.make_aes_key();
                //get the users public key
                if(typeof invitee === 'string'){
                    return this._user_lookup(invitee);
                } else {
                    return this._get_user_public_key(invitee);
                }
            }).then((usersPubKey:any)=>{
                let key:string;
                //get the encrypted aes key
                if(typeof usersPubKey !== 'string'){
                    invitee = usersPubKey.id;
                    key = usersPubKey.public_key;
                } else {
                    key = usersPubKey;
                }
                let encrypted_group_aes_key:string = this.asym.get_encrypted_aes_key(key, true);
                //encrypt the group private key
                let encrypted_group_private_key:string = this.asym.encrypt(group_private_key);
                let data:({
                    user:number,
                    encrypted_aes_key:string,
                    encrypted_private_key:string
                }) = {
                    user: invitee,
                    encrypted_aes_key: encrypted_group_aes_key,
                    encrypted_private_key: encrypted_group_private_key
                };
                this.asym.load();
                return this.api.jsonPost(`/api/groups/${group_id}/users/`, data);
            }).then((res)=>{
                resolve(res);
            }).catch((err)=>{
                if(err.res){
                    reject(err);
                } else {
                    reject(new Exception(err.json(), 'Unable to add the user to the group'));
                }
            });
        });
    }

    /**
     * Send secrets to a server
     * @param {string} name - the name of the secrets
     * @param {string} type - the type of the secrets
     * @param {object} visible_data - the visible info about the secrets
     * @param {object} secrets - the secrets to be encrypted
     * @param {number} group_id - the group id to use the pub key from if any
     * @return {Promise} a promise with the response
     */
    public create_secret(name:string, type:string, visible_data:any, secrets:any, group_id?:number): Promise<Response> {
        return new Promise((resolve, reject)=>{
            /**
             * Send the new secret to the server
             * @param {secret[]} secrets - the array of secrets
             * @return {Promise<Response>} the response of the request
             */
            const sendSecret = (secrets:Secret[]): Promise<Response> =>{
                //create the data object to send
                let data:NewSecret = {
                    name,
                    type,
                    visible_data,
                    secrets
                };
                return this.api.jsonPost('/api/secrets/', data);
            };
            //get the group public key if there is a group id
            if(group_id){
                this._get_group_public_key(group_id).then((pubKey)=>{
                    let encObj:({encrypted_key:string, encrypted_secrets:any}) = this._encrypt_secrets(secrets, pubKey);          
                    sendSecret([{group: group_id, hidden_data: encObj.encrypted_secrets, encrypted_key: encObj.encrypted_key}]).then((res)=>{
                        resolve(res);
                    }).catch((err)=>{
                        reject(new Exception(err.json(), 'Unable to add secret'));
                    });
                });
            } else {
                //encrypt the secrets with the users public key
                let encObj:({encrypted_key:string, encrypted_secrets:any}) = this._encrypt_secrets(secrets);          
                sendSecret([{hidden_data: encObj.encrypted_secrets, encrypted_key: encObj.encrypted_key}]).then((res)=>{
                    resolve(res);
                }).catch((err)=>{
                    reject(new Exception(err.json(), 'Unable to add secret'));
                });
            }
        });
    }

    /**
     * Update a secret
     * @TODO
     * @param {number} unique_secret_id - the id of the secret to update
     * @param {string} name - the new name of the secret
     * @param {object} visible_data - the visible data of the secret in key/value format
     * @param {object} secrets - the secrets to be encrypted in key/value format
     * @param {number} group_id
     * @return {Promise<Response>} returns the response of the ajax call
     */
    public update_secret(unique_secret_id:number, name?:string, visible_data?:any, secrets?:any, group_id?:number): Promise<Response> {
        return new Promise((resolve, reject)=>{
            if(!secrets){
                let patch_data:any = {};
                if(name){
                    patch_data.name = name;
                }
                if(visible_data){
                    patch_data.visible_data = visible_data;
                }
                this.api.jsonPatch(`/api/secrets/${unique_secret_id}/`, patch_data).then((res)=>{
                    resolve(res);
                }, (err)=>{
                    reject(new Exception(err.json(), "Could't patch secret"));
                });
            } else {
                //get the secret
                this.get_secret(unique_secret_id).then((unique_secret:NewSecret)=>{
                    unique_secret.secrets = unique_secret.secrets.map((secret)=>{
                        let encObj:({encrypted_key:string, encrypted_secrets:any}) = this._encrypt_secrets(secrets, secret.public_key);
                        secret.hidden_data = encObj.encrypted_secrets;
                        secret.encrypted_key = encObj.encrypted_key;
                        return secret;
                    });
                    //the data we are sending to the server
                    let data:NewSecret = {
                        name: name,
                        type: unique_secret.type,
                        visible_data: visible_data,
                        secrets: unique_secret.secrets
                    };
                    return this.api.jsonPut('/api/secrets', data);
                }).then((res)=>{
                    resolve(res);
                }).catch((err)=>{
                    reject(new Exception(err.json(), "Couldn\'t update secret"));
                });
            }
        });
    }

    /**
     * Get a secret by its id
     * @param {number} unique_secret_id - the id of the secret to get
     * @return {Promise<Object>} a promise with the secret data
     */
    public get_secret(unique_secret_id:number): Promise<NewSecret> {
        return new Promise((resolve, reject)=>{
            this.api.jsonGet(`/api/secrets/${unique_secret_id}/`).then((res)=>{
                resolve(res.json());
            }, (err)=>{
                reject(new Exception(err.json(), "Couldn't get secret"));
            });
        });
    }

    /**
     * Get the users list of secrets
     * @return {Promise<Object>} a promise with the list in an object
     */
    public list_secrets(): Promise<any> {
        return new Promise((resolve, reject)=>{
            this.api.jsonGet('/api/secrets/').then((res)=>{
                resolve(res.json());
            }, (err)=>{
                reject(new Exception(err.json(), "Couldn't get secrets"));
            });
        });
    }

    /**
     * Decrypt a secret object
     * @param {NewSecret} unique_secrets - the secret object returned from the server
     * @return {Promise<object>} a promise with the decrypted secrets in key/value format
     */
    public decrypt_secret(unique_secret:NewSecret): Promise<any> {
        return new Promise((resolve, reject)=>{
            let secret:Secret = unique_secret.secrets[0];
            if(secret.group){
                //get the group private key
                this._get_group_private_key(secret.group).then((privKey)=>{
                    resolve(this._decrypt_secrets(secret.encrypted_key, secret.hidden_data, privKey));
                }, (err)=>{
                    reject(err);
                });
            } else {
                //decrypt
                resolve(this._decrypt_secrets(secret.encrypted_key, secret.hidden_data));
            }
        });
    }

    /**
     * Add a group to a secret
     * @param {number} group_id - the id of the group
     * @param {object} secrets - the secrets to encrypt in key/value
     * @param {number} unique_secret_id - the id of the secret to add the group to
     * @return {Promise<Response>} a promise with the response of the request
     */
    public add_group_to_secret(group_id:number, secrets:any, unique_secret_id:number): Promise<Response> {
        return new Promise((resolve, reject)=>{
            this._get_group_public_key(group_id).then((pubKey)=>{
                let encObj:({encrypted_key:string, encrypted_secrets:any}) = this._encrypt_secrets(secrets);          
                //the data to be sent to the server
                let data:({
                    group:number,
                    encrypted_key:string,
                    hidden_data:any
                }) = {
                    group: group_id,
                    encrypted_key: encObj.encrypted_key,
                    hidden_data: encObj.encrypted_secrets
                };
                return this.api.jsonPost(`/api/secrets/${unique_secret_id}/groups/`, data);
            }).then((res)=>{
                resolve(res);
            }).catch((err)=>{
                if(err.res){
                    reject(err);
                } else {
                    reject(new Exception(err.json(), 'Unable to add group to secret'));
                }
            });
        });
    }

}

//add to the global scope
declare var window:any;
if(window){
    window.passitSDK = passitSDK;
}


/**
 * The definition for the config types
 */
export interface ConfigObj {

    URL: string,

    DEFAULT_HASH_ITERATIONS: number,

    MAXKEYS:number,
    
    KEYBITS:number,

    WORKERFILE:string

}

/**
 * The default options for the sdk
 */
export var Config:ConfigObj = {

    /**
     * The default url for the sdk
     * @type {string}
     */
    URL: 'http://0.0.0.0:8000',

    /**
     * The default hash iterations
     * @type {number}
     */
    DEFAULT_HASH_ITERATIONS: 24000,

    /**
     * The maximum number of keys to generate at once
     * @type {number}
     */
    MAXKEYS: 3,

    /**
     * The bits to use for generating keys
     * @type {number}
     */
    KEYBITS: 4096,

    /**
     * The location of the web worker script
     * @type {string}
     */
    WORKERFILE: '/dist/key_pair.js'


};

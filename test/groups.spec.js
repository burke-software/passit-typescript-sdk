
//tests for the groups

describe('Group Tests', function(){
    var accounts;
    //the group id
    var groupId;
    //create the accounts for the tests
    before(function(done){
        this.timeout(100000);
        createAccounts.apply(this, generateEmails('@test.com', 3)).then((act)=>{
            accounts = act;
            done();
        });
    });

    it('Should Create A Group', function(done){
        this.timeout(100000);
        accounts.get(0).create_group.apply(accounts.get(0), createGroupNameAndSlug()).then((res)=>{
            res = res.json();
            groupId = res.id;
            expect(res.id).to.be.a('number');
            done();
        }, (err)=>{
            console.dir(err);
            expect(true).to.be.equal(false);
            done();
        });
    });

    it('Should Add Users To Group', function(done){
        this.timeout(100000);
        accounts.get(0).add_user_to_group(groupId, accounts.email(1)).then((res)=>{
            res = res.json();
            expect(res.access_group).to.be.a('number');
            done();
        }, (err)=>{
            console.dir(err);
            expect(true).to.be.equal(false);
            done();
        });
    });

});


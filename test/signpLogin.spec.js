
/**
 * Test The Sign Up
 */
var email = "";
var pass = "testpassword";

var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
for(var i=0; i<10; i++){
    email += possible.charAt(Math.floor(Math.random() * possible.length));    
}

email += "@test.com";

describe("Sign Up", function(){
    it("Should Be Able To Sign Up", function(done){
        this.timeout(100000);
        var sdk = new passitSDK();
        sdk.sign_up(email, pass).then((id)=>{
            expect(id).to.be.a('number');
            done();
        }).catch((err)=>{
            console.dir(err);
            expect(true).to.be.equal(false);
            done();
        });
    });
});

describe("Log In", function(){
    it("Should Be Ablt To Log In", function(done){
        this.timeout(100000);
        var sdk = new passitSDK();
        sdk.log_in(email, pass).then(()=>{
            expect(sdk.api.token.length).to.be.above(10);
            done();
        }).catch((err)=>{
            console.dir(err);
            expect(true).to.be.equal(false);
            done();
        });
    });
});


/**
 * Function for creating user accounts for testing
 * @param {...string} email - the email accounts to use
 * @return {Promise<Object>} key value object of email/instance
 */
var createAccounts = function(email){
    var emails = Array.from(arguments);
    var accounts = {};
    /**
     * A function for getting an account at an index
     * @param {number} idx - the index of the email address
     * @return {passitSDK} the sdk instance of that email
     */
    Object.defineProperty(accounts, 'get', {
        value: function(idx){
            var keys = Object.keys(accounts);
            return accounts[keys[idx]];
        }
    });
    /**
     * Get the email account at an index
     * @param {number} idx - the index of the email address
     * @return {string} the email address
     */
    Object.defineProperty(accounts, 'email', {
        value: function(idx){
            var keys = Object.keys(accounts);
            return keys[idx];
        }
    });
    return new Promise((resolve, reject)=>{
        /**
         * Function for creating the account
         */
        var create = () =>{
            if(emails.length === 0){
                return resolve(accounts);
            }
            var email = emails.shift();
            //create the new instance
            accounts[email] = new passitSDK();
            //signup and login
            accounts[email].sign_up(email, 'testpass').then((id)=>{
                accounts[email].log_in(email, 'testpass').then(()=>{
                    create();
                });
            });

        };
        create();
    });
};

/**
 * Generate random emails
 * @param {string} end - the email ending string
 * @param {number} count - the total number of emails to create
 */
var generateEmails = function(end, count){
    var emails = [];
    for(var i=0; i<count; i++){
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        emails[i] = "";
        for(var x=0; x<10; x++){
            emails[i] += possible.charAt(Math.floor(Math.random() * possible.length));    
        }
        emails[i] += end;
    }
    return emails;
};

/**
 * Generate a random group name and slug
 * @return {array} an array with the first item being the name and the second being the slug
 */
var createGroupNameAndSlug = function(){
    var name = "";
    var possible = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i=0; i<20; i++){
        name += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    var slug = name.replace(' ','-').toLowerCase();
    return [name, slug];
};

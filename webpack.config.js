
const webpack = require('webpack');

module.exports = {

    entry: './src/sdk.ts',
    output: {
        path: './dist/',
        filename: 'passitSDK.js',
        libary: 'passitSDK',
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
    },
    module: {
        loaders: [
            { test: /\.ts$/, loader: 'ts-loader' },
            { test: /\.js$/, loader: 'script' }
        ]
    }

};
